import java.util.List;

public class ListaResultados {
    private List<Object> listaResultado;
    private List<Object> listaFiltrados;

    public ListaResultados(List<Object> listaResultado, List<Object> listaFiltrados) {
        this.listaResultado = listaResultado;
        this.listaFiltrados = listaFiltrados;
    }

    public List<Object> getListaResultado() {
        return listaResultado;
    }

    public List<Object> getListaFiltrados() {
        return listaFiltrados;
    }
}

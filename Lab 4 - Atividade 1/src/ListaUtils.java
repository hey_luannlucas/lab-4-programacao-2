import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListaUtils {
    public static ListaResultados removerDuplicados(List<Object> lista) {
        List<Object> listaResultado = new ArrayList<>();
        List<Object> listaFiltrados = new ArrayList<>();
        Set<Object> elementosSet = new HashSet<>();

        for (Object elemento : lista) {
            if (!elementosSet.add(elemento)) {
                listaFiltrados.add(elemento);
            } else {
                listaResultado.add(elemento);
            }
        }

        return new ListaResultados(listaResultado, listaFiltrados);
    }
}

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Object> listaEntrada = Arrays.asList(1, 2, 3, 3, 4, 4, 5);
        List<Object> listaEntrada2 = Arrays.asList('a', 'b', 'b', 'f', 'c', 'v', 'x', 'c');

        ListaResultados resultados1 = ListaUtils.removerDuplicados(listaEntrada);
        ListaResultados resultados2 = ListaUtils.removerDuplicados(listaEntrada2);

        System.out.println("Resultado (Lista 1): " + resultados1.getListaResultado());
        System.out.println("Filtrados (Lista 2): " + resultados1.getListaFiltrados());
        System.out.println();
        System.out.println("Resultado (Lista 1): " + resultados2.getListaResultado());
        System.out.println("Filtrados (Lista 2): " + resultados2.getListaFiltrados());
    }
}

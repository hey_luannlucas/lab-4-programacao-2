# Remover Duplicados em Listas (Java)

Este é um aplicativo Java que permite remover elementos duplicados de uma lista (do tipo ArrayList ou LinkedList) e armazená-los em uma segunda lista. O programa apresenta duas classes principais: Main e ListaUtils.

## Como funciona:

A classe `ListaUtils` contém o método estático `removerDuplicados`, que recebe uma lista como entrada e retorna um objeto da classe `ListaResultados`.
O método removerDuplicados percorre a lista de entrada, mantendo um conjunto auxiliar (Set) para rastrear os elementos únicos.
Se o elemento já estiver presente no conjunto, ele é adicionado à lista de elementos duplicados (listaFiltrados).
Caso contrário, o elemento é adicionado à lista de elementos únicos (listaResultado).
O método retorna o objeto ListaResultados, contendo as duas listas resultantes.
Exemplo de Saída:

## Diagrama de classe:
![](Assets/diagrama%20de%20classes.png)

## Executando:
![](Assets/execucao.png)
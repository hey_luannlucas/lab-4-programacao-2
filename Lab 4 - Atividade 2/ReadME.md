# Matriz Genérica em Java
Este é um aplicativo Java que implementa uma classe genérica de matriz, permitindo o armazenamento de matrizes NxN com qualquer tipo de dado (Char, Int, Double, String).
***

## Classe MatrizGenerica
A classe MatrizGenerica é uma matriz genérica que utiliza o recurso de Generics (parametrização de tipos) para permitir a utilização de qualquer tipo de dado na matriz.

### Construtor:
O construtor da classe MatrizGenerica é responsável por criar uma nova matriz genérica e aceita uma matriz bidimensional como parâmetro.

```java
public MatrizGenerica(T[][] matriz) {
    // Implementação do construtor
}
```
### Métodos:
A classe MatrizGenerica possui os seguintes métodos para manipulação da matriz:

- listarDados(): Percorre a matriz da esquerda para a direita e de cima para baixo, imprimindo uma linha da matriz por vez.

```java
public void listarDados() {
    // Implementação do método listarDados
}
```
- imprimirDiagonalPrincipal(): Imprime todos os elementos da diagonal principal, ou seja, os elementos que possuem índices iguais, linha == coluna.
```java
public void imprimirDiagonalPrincipal() {
    // Implementação do método imprimirDiagonalPrincipal
}
```
- matrizTransposta(): Desfaz a ordem da matriz, transformando as linhas em colunas e vice-versa.
```java
public void matrizTransposta() {
    // Implementação do método matrizTransposta
}
```

## Classe Main
A classe Main contém o método main, que é o ponto de entrada do programa. Neste exemplo, criamos matrizes de caracteres e inteiros para demonstrar o funcionamento da classe MatrizGenerica e suas funcionalidades.

```java
public class Main {
    public static void main(String[] args) {
        // Criação e manipulação das matrizes de caracteres e inteiros
    }
}
```

### Diagrama de classes:
![diagrama](Assets/Diagrama%20de%20classes.png)

### Execução do Programa
Ao executar o programa, ele imprimirá as matrizes originais, a diagonal principal e a matriz transposta para cada uma das matrizes de caracteres e inteiros de exemplo.

Exemplo de Saída:

![execucao](Assets/execucao.png)
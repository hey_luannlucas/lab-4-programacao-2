public class Main {
    public static void main(String[] args) {
        // Exemplo de matriz de caracteres
        Character[][] matrizCaracteres = {
                {'a', 'b', 'c'},
                {'d', 'r', 'f'},
                {'g', 'h', 'i'}
        };

        MatrizGenerica<Character> matrizGenerica1 = new MatrizGenerica<>(matrizCaracteres);
        System.out.println("Matriz quadrada de ordem " + matrizCaracteres.length + " de caracteres");
        matrizGenerica1.listarDados();
        System.out.print("Diagonal Principal: ");
        matrizGenerica1.imprimirDiagonalPrincipal();
        matrizGenerica1.matrizTransposta();
        System.out.println("Matriz Transposta:");
        matrizGenerica1.listarDados();
        System.out.println();

        // Exemplo de matriz de inteiros
        Integer[][] matrizInteiros = {
                {2, 3, 5},
                {7, 8, 9},
                {1, 5, 4}
        };

        MatrizGenerica<Integer> matrizGenerica2 = new MatrizGenerica<>(matrizInteiros);
        System.out.println("Matriz quadrada de ordem " + matrizInteiros.length + " de inteiros");
        matrizGenerica2.listarDados();
        System.out.print("Diagonal Principal: ");
        matrizGenerica2.imprimirDiagonalPrincipal();
        matrizGenerica2.matrizTransposta();
        System.out.println("Matriz Transposta:");
        matrizGenerica2.listarDados();
    }
}

import java.util.Arrays;

public class MatrizGenerica<T> {
    private T[][] matriz;

    public MatrizGenerica(T[][] matriz) {
        this.matriz = matriz;
    }

    public void listarDados() {
        for (T[] row : matriz) {
            System.out.println(Arrays.toString(row));
        }
    }

    public void imprimirDiagonalPrincipal() {
        int n = matriz.length;
        for (int i = 0; i < n; i++) {
            System.out.print(matriz[i][i] + " ");
        }
        System.out.println();
    }

    public void matrizTransposta() {
        int n = matriz.length;
        T[][] transposta = (T[][]) new Object[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                transposta[i][j] = matriz[j][i];
            }
        }

        matriz = transposta;
    }
}

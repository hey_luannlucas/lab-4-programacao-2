# Laboratório Semana 4 - <a href="https://gitlab.com/hey_luannlucas">Luann Lucas</a>

Aqui você encontrará minhas soluções para o Laboratório da semana 4 de programação 2

### Estrutura de Pastas 📁

Cada resolução se encontra na sua pasta correspondente ao nome do arquivo.

### ReadME 🗒️
Cada pasta (solução) possui seu arquivo README com explicações e diagramas. As imagens estão localizadas na pasta "Assets", mas elas já estão incluídas no arquivo README.
***
<br>


<div class="links-container" style="display: flex;">
  <div class="email-link" style="margin-right: 20px;">
    <a href="mailto:Luann.deSousa@jala.university" style="display: flex; align-items: center; text-decoration: none;">
      <img src="Lab%204%20-%20Atividade%201/Assets/outlook.svg" alt="outlook" width="60" height="60" style="margin-right: 10px; border-radius: 10%;">
    </a>
  </div>
